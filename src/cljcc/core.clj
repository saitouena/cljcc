(ns cljcc.core
  (:require [clojure.java.io :as io])
  (:import (java.io PushbackReader StringReader))
  (:gen-class))

(def digits #{\0 \1 \2 \3 \4 \5 \6 \7 \8 \9})
(def one-nine #{\1 \2 \3 \4 \5 \6 \7 \8 \9})

(defn peek-rdr
  [^PushbackReader rdr]
  (let [ch (.read rdr)]
    (when (<= 0 ch)
      (.unread rdr ch)
      (char ch))))

(defn rdr-eof?
  [^PushbackReader rdr]
  (let [ch (.read rdr)]
    (if (<= 0 ch)
      (do
        (.unread rdr ch)
        false)
      true)))

(let [rdr (PushbackReader. (StringReader. ""))]
  (.read rdr)
  (.read rdr))

(defn digit->int
  [d]
  (- (int d) (int \0)))

(comment
  (digit->int \9))

;; int = digit | one-nine digit*
(defn read-int
  [^PushbackReader rdr]
  (let [ch (peek-rdr rdr)]
    (when (and ch (digits ch))
      (.skip rdr 1)
      (if (= \0 ch)
        0
        (loop [res (digit->int ch)
               rdr rdr]
          (let [ch (peek-rdr rdr)]
            (if (and ch (digits ch))
              (do
                (.skip rdr 1)
                (recur (+ (* 10 res) (digit->int ch)) rdr))
              res)))))))

(comment
  (let [rdr (PushbackReader. (StringReader. "0"))]
    (read-int rdr))
  (let [rdr (PushbackReader. (StringReader. "1"))]
    (read-int rdr))
  (let [rdr (PushbackReader. (StringReader. "10"))
        [_ rdr] (read-int rdr)]
    (.read rdr))
  (let [rdr (PushbackReader. (StringReader. "1000"))]
    (read-int rdr)))

(defn emit
  [expr]
  (let [rdr (PushbackReader. (StringReader. expr))
        n (read-int rdr)]
    (println ".intel_syntax noprefix")
    (println ".global main")
    (println "main:")
    (println (format "  mov rax, %d" n))
    (while (not (rdr-eof? rdr))
      (let [op (char (.read rdr))
            n (read-int rdr)
            fstr (case op
                   \+ "  add rax, %d"
                   \- "  sub rax, %d")]
        (println (format fstr n)))))
  (println "  ret"))

(defn -main
  [expr]
  (spit "out.s"
   (with-out-str
     (emit expr))))

(let [rdr (PushbackReader. (StringReader. "10"))]
  (read-int rdr)
  (rdr-eof? rdr))

(comment
  (-main "1")
  (emit "100")
  (emit "100+1")
  (emit "100+1+1")
  (emit "100+1+23")
  (emit "100+1-10")
  (let [rdr (PushbackReader. (StringReader. "hoge"))]
    (char (.read rdr))
    (char (.read rdr))
    #_(.unread rdr (int \h))
    #_(.unread rdr (int \h))
    (char (.read rdr))
    (char (.read rdr))
    (.ready rdr)
    (char (.read rdr))))
