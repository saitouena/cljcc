(ns run-test
  (:require [cljcc.core :as c]
            [clojure.java.shell :as sh]))

(defn- run-test
  [expr expected]
  (c/-main expr)
  (sh/sh "gcc" "-o" "out" "out.s")
  (let [res (sh/sh "./out")]
    (if (= expected (:exit res))
      (println expr " => " expected)
      (println expr " => " expected " expected, but got " (:exit res)))))

;; tests
(run-test "0" 0)
(run-test "42" 42)

(run-test "1+2+3" 6)
(run-test "5+20-4" 21)
